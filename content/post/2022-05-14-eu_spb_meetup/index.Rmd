---
title: "14 May 2022 EU SPb meetup"
date: 2022-05-14
categories: ["eu spb"]
tags: ["packages", "sf", "osmdata", "geospatial data"]
---

# Митя Серебренников, "Основы работы с пространственными данными в R"

<iframe width="560" height="315" src="https://www.youtube.com/embed/mAIn5jQnhWM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>

</iframe>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, warning = FALSE, message = FALSE, out.width = '60%')
```

## План митапа

-   Типы пространственных данных;

-   Особенности геоданных;

-   R и пространственный анализ;

-   Практика - находим геоданные разных форматов и учимся с ними работать;

-   Бонус

-   Что можно почитать и посмотреть по теме.

Загрузим библиотеки

```{r}
# Функция для загрузки\установки библиотек
package_loader <- function(pkgs) {
  if (length(setdiff(pkgs, rownames(installed.packages()))) > 0) {
    install.packages(setdiff(pkgs, rownames(installed.packages())))
  }
  lapply(pkgs, library, character.only = T)
}

# Загружаем библиотеки
package_loader(c('data.table', 'dplyr', 'readr', 'sf', 'geojsonsf', 'geojsonio', 'rnaturalearth', 'osmdata', 'mapview'))
```

## 1. Типы пространственных данных

**а. Векторные данные**

-   Геометрии состоят из точек. Конвенционально x = широта, y = долгота. Кроме этого могут использоваться z = высота, M = мера изменчивости точки;

-   Виды геометрий:

<!-- ```{r, eval=FALSE, include=FALSE} -->
<!-- knitr::include_graphics("geom_types.png") -->
<!-- ``` -->
![](geom_types.png){width=500px}

+++ GEOMETRYCOLLECTION +++

-   Форматы записи данных:

    *Well-Known Text (WKT)* - Запись точек вектором из геометрий

    *Well-Known Binary (WKB)* - Запись координат в бинарных значениях. Используется в базах данных (т.к. увеличивает скорость работы с данными), но нечитаем и непонятен для человека.

Посмотрим в первом приближении:

```{r}
# library(rnaturalearth)
world <- ne_countries(returnclass = "sf")
world[1:5, ]

# Для иллюстрации
world <- world[1:5, c(4, ncol(world))]
world
as.data.frame(world)
```

**b. Растровые данные**

-   Матрица значений пикселей пространственной области. В растрах хронят космические снимки и базы различных геологических данных. Также как и вектор - предназначены для определённого круга задач;

-   Основной формат: .tif

-   Основные библиотеки для обработки в R: raster и stars

*Сравним:*

<!-- ```{r} -->
<!-- knitr::include_graphics("vactor-raster.png") -->
<!-- ``` -->
![](vactor-raster.png){width=500px}

**На этом и последующих занятиях мы будем говорить только о векторных данных.**

Главное, нужно помнить, что векторные пространственные данные - это (чаще всего) просто точки на плоскости! Создадим собственные пространственные данные:

```{r}
p1 = st_point(c(7, 52)) # Рисуем точку 1
p2 = st_point(c(-30, 20)) # Рисуем точку 2
sp_obj = st_sfc(p1, p2, crs = 4326) # Преобразуем в пространственные данные
plot(sp_obj)

# Вы восхитительны!
```

## 2. Особенности геоданных

Вернёмся к векторным геометриям:

```{r}
world
```

Что такое CRS?

**Coordinate Reference Systems** (CRS) / Пространственная привязка и её **проекции**

Для иллюстрации обратимся к [прекрасной работе Тасс](https://merkator.tass.ru/).

Картинки чтобы окончательно закрепить идею проекции:

<!-- ```{r} -->
<!-- knitr::include_graphics("mercator_apple.jpg") -->
<!-- ``` -->
![](mercator_apple.jpg){width=500px}
<!-- ```{r} -->
<!-- knitr::include_graphics("proj_man.jpg") -->
<!-- ``` -->
![](proj_man.jpg){width=500px}

Как посмотреть CRS?

```{r}
st_crs(world)
```

<!-- ```{r} -->
<!-- knitr::include_graphics("slozhno.jpg") -->
<!-- ``` -->
![](slozhno.jpg){width=500px}


На самом деле всё не так сложно.

Пространственный объект можно перевести в другую CRS сразу и быстро, задав через st_transform() нужную проекцию одним из четырёх форматов (просто гуглите то, что нужно для ваших координат):

-   Код EPSG (например, "4326" - универсальная проекция для всего мира или "31370" - кастомная проекция для Бельгии):

```{r, eval=FALSE}
df <- st_as_sf(df) # <- убедитесь, что ваш объект типа "sf"
df <- st_transform(df,
                   crs = 4326)
```

-   Формула PROJ4 (например, "+proj=longlat +datum=WGS84 +no_defs"):

```{r, eval=FALSE}
df <- st_transform(df,
                   crs = "+proj=longlat +datum=WGS84 +no_defs")
```

-   Просто переносом проекции с другого пространственного объекта в формате:

```{r, eval=FALSE}
df1 <- st_transform(df1, 
                    crs = st_crs(df2))
```

**Примеры**

```{r}
Afg = world[1,]
plot(Afg)
```

```{r}
# Изменим crs
Afg <- st_transform(Afg, 2264)
plot(Afg)
```

```{r}
# Приведём crs к стандартному формату
Afg <- st_transform(Afg, crs = "+proj=longlat +datum=WGS84 +no_defs")
plot(Afg)
```

...В геоданных есть ещё множество подводных камней, но этот - ключевой и его необходимо запомнить...

## 3. R и пространственный анализ

**Почему R?**

<!-- ```{r} -->
<!-- knitr::include_graphics("30_days_chellenge.png") -->
<!-- ``` -->
![](30_days_chellenge.png)

*Библиотеки для работы с геоданными в R*

(несть им числа, но основные)

-   [sf](https://r-spatial.github.io/sf/) - state-of-the-art пространственного анализа в R.

Немного истории:

<!-- ```{r} -->
<!-- knitr::include_graphics("sf_architecture.png") -->
<!-- ``` -->
![](sf_architecture.png)

## 4. Практика

Что мы будем делать - делать карту мороженного в Израиле! :)

...

Я исхожу из того, что вы знаете dplyr или data.table на базовом уровне, но если у вас вызывает затруднения именно манипуляции с данными - обязательно задавайте вопросы!

Мы отработаем работу с данными на трёх основных форматах для векторных данных: \* csv \* geojson \* shape-file

**A. Датасет с мороженками в csv**

Загрузим данные. К сожалению, в R есть известная проблема с кодировками и чтобы долго не превращать крокозябры в иврит - уберём колонки названий точек мороженного:

```{r}
isr_icecream <- read.csv('https://raw.githubusercontent.com/AmitLevinson/Datasets/master/golda/golda_locations.csv')
isr_icecream <- isr_icecream |> 
  select(-c('city', 'street'))
class(isr_icecream)
```

Превратим тип данных "data.frame" в "sf":

```{r}
isr_icecream <- st_as_sf(
  isr_icecream, 
  dim = "XY", 
  remove = T, 
  na.fail = F, 
  coords = c("lon", "lat"), 
  crs = "+proj=longlat +datum=WGS84 +no_defs")
isr_icecream

plot(isr_icecream[1])
```

**B. Границы Израиля в Shape-file**

Идём [сюда](http://www.diva-gis.org/gdata), ищем Израиль и загружаем архив. Что мы видим внутри?

.shp -- файл с геометриями, которые мы подгружаем через st_read()

.dbf

.shx

.prj

Все они важны! По этой причине, когда вам нужно переслать shape-file отправляйте архив из всех четырёх файлов.

Загрузим его и отобразим:

```{r}
isr_border <- st_read('ISR_adm/ISR_adm0.shp')
isr_border

plot(isr_border)
plot(isr_border[1])
plot(st_geometry(isr_border))
```

**C. Точки городов Израиля в GeoJson**

Json с геоданными. Достаточно тяжёлый с точки зрения размещения данных, но часто встречающийся формат.

Здесь стоит отвлечься и ознакомиться с "Википедией"" от мира пространственных данных - [Open Street Map](https://www.openstreetmap.org/).

Если это "Википедия", то с неё можно выкачать данные? Да. По тэгам...

Возьмём [тэги городов](https://wiki.openstreetmap.org/wiki/Tag:place%3Dcity).

Скачаем GeoJson с городами. Загрузим его. Внутри будет огромное количество колонок, которые сейчас нам не нужны. Оставим первую и последнюю:

```{r}
isr_city <- st_read('city.geojson')
isr_city <-  isr_city[,c(1,ncol(isr_city))] 
isr_city

plot(isr_city)
```

Альтернативный способ через пакет osmdata

```{r}
# library(osmdata)

admin_osm = opq(bbox = 'Sankt-Peterburg') %>% # Попробуйте также 'Sankt-Peterburg' для примера
  add_osm_feature(key = "admin_level", value = '5') %>%
  osmdata_sf()

# Но есть проблемы с кодировкой
iconv(admin_osm$nodes$tags, from="UTF-8", to="UTF-8") 
iconv(admin_osm$nodes$tags, from="UTF-8", to="cp1251")
```

```{r}
plot(admin_osm[["osm_multipolygons"]])
```

**Объединяем слои и строим свою первую карту!**

```{r}
# Устанавливаем одинаковые проекции для всех объектов: 
isr_border <- st_transform(isr_border, 4326)
isr_city = st_transform(isr_city, st_crs(isr_border))
isr_icecream = st_transform(isr_icecream, st_crs(isr_border))

# Делаем карту (st_geometry() - вывести чистую геометрию, без других колонок или значений в объекте)
{plot(isr_border %>% st_geometry())
plot(isr_city, col = 'red', add = T)
plot(isr_icecream, col = 'blue', add = T)}
```

**Ура :)**

Теперь сохраним данные (здесь есть свои подводные камни):

```{r}
write_sf(isr_border, 
         "meetup_results_folder/isr_border.shp", 
         append = T, 
         layer_options = "ENCODING=UTF-8" # При сохранении данных с не-латинскими character обязательно прописывайте кодировку
         )
```

Загрузим в качестве проверки и убедимся, что всё работает

```{r}
double_isr_border <- st_read("meetup_results_folder/isr_border.shp")
plot(st_geometry(double_isr_border))
```

## 5. Бонус

Построим интерактивный график с помощью очень простой по своему синтаксису библиотеки mapview. Туториал по ней можно найти [здесь](https://bookdown.org/nicohahn/making_maps_with_r5/docs/mapview.html). mapview предназначен для карт небольшого размера.

**ЗАПУСКАТЬ ОСТОРОЖНО!!!** (на Windows возможны вылеты RStudio - сохраните код перед запуском, чтобы понять, корректно ли установился пакет или нет, если нет - переустановите из исходников)

```{r}
# library(mapview)
mapview(isr_border) + 
  mapview(isr_city %>% st_geometry(), col.regions = 'red', legend = FALSE) +
  mapview(isr_icecream %>% st_geometry(), col.regions = 'blue', legend = FALSE)
```

## 6. Где брать данные?

-   Open street map

-   naturalearthdata.com/downloads/

-   download.geofabrik.de/

-   [\#30DaysMapChallenge](https://github.com/tjukanovt/30DayMapChallenge)

-   [Один из многочисленных обзоров челленджа](https://rud.is/books/30-day-map-challenge/)

...и много, много других ресурсов...

## 7. Что почитать?

[Самсонов Т.Е. - Визуализация и анализ географических данных на языке R](https://tsamsonov.github.io/r-geo-course/)

[Pebesma E., Bivand R. Spatial Data Science. 2020](https://keen-swartz-3146c4.netlify.app/).

[Lovelace R., Nowosad J., Muenchow J. Geocomputation with R. 2021](https://geocompr.robinlovelace.net/)

[Dorman M. Using R for Spatial Data Analysis. 2021](https://michaeldorman.github.io/R-Spatial-Workshop-at-CBS-2021/main.html#Setup:_sample_data).

...

Можете посмотреть другие материалы из нашей подборки в [закрепе Горячей линии](https://t.me/hotlineR_EU/7692)
